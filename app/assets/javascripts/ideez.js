$(document).ready(function () {
    $('.countries_select').select2();
    $('.states_select').select2();
    $('.cities_select').select2();
    $('.sexe_select').select2();
    $('.pro_profile_select').select2();
    $('.box_select').select2();
    $('.countries_select').on('change', function (event) {
       console.log('country changed');
      updateStates(this);
    });

    updateStates($('.countries_select'));
    $('.states_select').on('change', function (event) {
        console.log('state changed');
       updateCities(this);
         console.log('clean selected');
           $('input[name="selectedState"]').val('');
    $('input[name="selectedCity"]').val('');
    });

    //updateCities($('.states_select'));



    const ideaLocalisationBtn = document.getElementById("icon-localisation");
    const ideaLocalisationForm = document.getElementById("idea-localisation");

    if (ideaLocalisationBtn) {
      ideaLocalisationBtn.addEventListener("click", function(){
       ideaLocalisationForm.style.display = ideaLocalisationForm.style.display === 'none' ? '' : 'none';
      });
    }


    $("#date-btn").on('click', function() {
      console.log('input[name="order"')
      $('input[name="order"]').val("date");
      $('#filter-form').submit();
    });

    $("#popularity-btn").on('click', function() {
      $('input[name="order"]').val("popularity");
      $('#filter-form').submit();
    });

    $("#rating-btn").on('click', function() {
      $('input[name="order"]').val("rating");
      $('#filter-form').submit();
    });
});
function isEmpty(value) {
  return value === undefined || value === null || value === "";
}
function updateStates(select) {
  if($(select).val() === null || $(select).val() === undefined) return;
        // console.log("CountryChange");
        const selectedState = $('input[name="selectedState"]').val();
        // console.log(selectedState);
        $('.states_select').empty();
        let option = new Option("Choisir votre région", "", isEmpty(selectedState), isEmpty(selectedState));
        //option.disabled = true;
        $('.states_select').append(option);
        $.getJSON("/states?country=" + $(select).val(), function (data) {
          for(var key in data) {
              $('.states_select').append(new Option(data[key], key, selectedState === key, selectedState === key ));
          }
          $('.states_select').trigger("change");
        });

}

function updateCities(select) {
   const selectedCity = $('input[name="selectedCity"]').val();
  $('.cities_select').empty();
   let option = new Option("Choisir votre ville", "", isEmpty(selectedCity), isEmpty(selectedCity));
  //option.disabled = true;
  $('.cities_select').append(option);

  if($(select).val() === null || $(select).val() === undefined) return;

  $.getJSON("/cities?state=" + $(select).val(), function (data) {
    // console.log('json return');
    for(var key in data) {
        $('.cities_select').append(new Option(data[key], data[key], selectedCity === data[key], selectedCity === data[key] ));
    }
  });
}
