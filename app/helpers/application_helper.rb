module ApplicationHelper

  # def svg(name)
  #   file_path = "#{Rails.root}/app/assets/images/#{name}.svg"
  #   if File.exists?(file_path)
  #     cache { File.read(file_path).html_safe }
  #   else
  #     '(not found)'
  #   end
  # end

  def show_svg(path)
    File.open("app/assets/images/#{path}", "rb") do |file|
      raw file.read
    end
  end

  def current_class?(test_path)
    return 'active' if request.fullpath == test_path
    ''
  end
end
