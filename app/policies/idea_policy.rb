class IdeaPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def show?
    if record.status == "publié"
      true
    elsif record.status == "brouillon" && record.profile.user == user
      true
    end
  end

  def create?
    true
  end

  def update?
    record.profile.user == user
  end

  def destroy?
    record.profile.user == user
  end

  def upvote?
    true
  end
end
