class ProProfile < ApplicationRecord
  belongs_to :user
  has_many :profile_tags
  has_many :ideas, through: :profile_tags

  mount_uploader :photo, PhotoUploader

  validates :title,
          :presence => {:message => "Vous devez ajouter un nom à votre entité" },
          :uniqueness => {:message => "Ce nom existe déjà"},
          :length => { :maximum => 40, :message => "Votre titre doit avoir moins de 40 caractères"}

end
