class Rate < ActiveRecord::Base
  belongs_to :rater, :class_name => "User"
  belongs_to :rateable, :polymorphic => true

  #attr_accessible :rate, :dimension

  def self.avg(idea)
    sum = 0
    number_of_rate = 0
    Rate.all.where(rateable_id: idea.id).each do |rate|
      sum += rate.stars
      number_of_rate += 1
    end
    if number_of_rate > 0
      stars_average = sum / number_of_rate
    else
      sum
    end
  end

  def self.score_range(idea)
    number_of_vote = Rate.where(rateable_id: idea.id).count
    score = 0
    if number_of_vote >= 0 && number_of_vote <= 10
      score = 0
    elsif number_of_vote > 10 && number_of_vote <= 20
      score = 0.5
    elsif number_of_vote > 20 && number_of_vote <= 40
      score = 1
    elsif number_of_vote > 40 && number_of_vote <= 60
      score =  1.5
    elsif number_of_vote > 60 && number_of_vote <= 80
      score = 2
    elsif number_of_vote > 80 && number_of_vote <= 100
      score = 2.5
    elsif number_of_vote > 100 && number_of_vote <= 120
      score = 3
    elsif number_of_vote > 120 && number_of_vote <= 140
      score = 3.5
    elsif number_of_vote > 140 && number_of_vote <= 160
      score = 4
    elsif number_of_vote > 160 && number_of_vote <= 200
      score = 4.5
    else
      score = 5
    end
  end
end