class Review < ApplicationRecord
  belongs_to :profile
  mount_uploader :photo, PhotoUploader

  validates :description, presence: true, length: { maximum: 240 }
end
