class Box < ApplicationRecord
  belongs_to :profile
  has_many :idea_boxes
  has_many :ideas

  validates :title,
          :presence => {:message => "Vous devez ajouter un nom à votre boite à idée" },
          :uniqueness => {:message => "Ce nom existe déjà"},
          :length => { :maximum => 40, :message => "Votre titre doit avoir moins de 40 caractères"}
end
