class Idea < ApplicationRecord
  belongs_to :profile
  has_many :reviews
  has_many :idea_boxes
  has_many :profile_tags
  has_many :boxes, through: :idea_boxes
  has_many :pro_profiles, through: :profile_tags
  has_many :bookmarks

  mount_uploader :photo, PhotoUploader

  ratyrate_rateable "rate"

  validates :title,
          :presence => {:message => "Vous devez ajouter un titre" },
          :uniqueness => {:message => "Ce titre existe déjà"},
          :length => { :maximum => 40, :message => "Votre titre doit avoir moins de 40 caractères"}
  validates :description,
          :presence => {:message => "Vous devez avoir une description" },
          :length => { :maximum => 240, :message => "Votre description doit avoir moins de 240 caractères"}
  validates :rating, presence: true, numericality: { only_integer: true }, inclusion: { in: [0,1,2,3,4,5] }

  STATUTES = ["brouillon", "publié"]
  validates :status, inclusion: {in: STATUTES}
end
