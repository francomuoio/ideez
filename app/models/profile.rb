class Profile < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :ideas
  has_many :ideas
  has_many :boxes
  has_many :bookmarks
  mount_uploader :photo, PhotoUploader


  validates :username,
          :presence => {:message => "Vous devez avoir un pseudo" },
          :uniqueness => {:message => "Ce pseudo existe déjà"},
          :length => { :maximum => 40, :message => "Votre pseudo doit avoir moins de 40 caractères"}
end
