import "bootstrap";

// Js for new idea

let wordCount = document.getElementById('wordCount');
let myText = document.getElementById('idea_description');

if(myText) {
  myText.addEventListener('keyup', function() {
    let characters = myText.value.split('');
    wordCount.innerText = characters.length + "/ 240";
    if (characters.length >= 240) {
      myText.style.borderBottom = "solid 1px red";
      myText.style.color = "red";
    } else {
      myText.style.borderBottom = "solid 1px #26A59A";
      myText.style.color = "black";
    }
  });

  myText.addEventListener('focusout', function() {
    myText.style.borderBottom = "solid 1px #f0f0f0;";
  });
}



