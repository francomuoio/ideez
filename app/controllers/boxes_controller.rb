class BoxesController < ApplicationController

  before_action :set_box, only: [:show, :edit, :update, :destroy]

  def index
    if params[:query].present?
      sql_query = " \
        boxes.title ILIKE :query \
        OR boxes.description ILIKE :query \
      "
      @boxes = policy_scope(Box).where(sql_query, query: "%#{params[:query]}%")
    else
      @boxes = policy_scope(Box)
    end
  end

  def show
    authorize @box
  end

  def new
    @box = Box.new
    authorize @box
  end

  def create
    @user = User.find(current_user.id)
    @profile = Profile.find_by(user_id: @user.id)
    @box = Box.new(box_params)
    authorize @box
    @box.profile_id = @profile.id
    if @box.valid?
      @box.save
      redirect_to box_path(@box)
    else
      render :new
    end
  end

  def edit
    authorize @box
  end

  def update
    @box.update(box_params)
    authorize @box
    if @box.valid?
      @box.save
      redirect_to box_path(@box)
    else
      render :new
    end
  end

  def destroy
    authorize @box
    @box.destroy
  end

  private

  def box_params
    params.require(:box).permit(:title, :description)
  end

  def set_box
    @box = Box.find(params[:id])
  end
end
