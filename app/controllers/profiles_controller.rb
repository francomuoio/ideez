class ProfilesController < ApplicationController

  before_action :set_profile, only: [:show, :edit, :update, :destroy, :bookmarks_page ]

  def index
    @profiles = policy_scope(Profile)
  end

  def my_profile
    @profile = Profile.find_by(user_id: current_user.id)
    authorize @profile
    if request.fullpath == "/my_profile" || request.fullpath == "/my_profile?ideas"
      @ideas = policy_scope(Idea).where(profile_id: @profile.id).where(status: "publié").order(created_at: :desc)
    elsif request.fullpath == "/my_profile?box_idea"
      @boxes = policy_scope(Box).where(profile_id: @profile.id)
    elsif request.fullpath == "/my_profile?pro_profiles"
      @pro_profiles = policy_scope(ProProfile).where(user_id: User.find(@profile.user_id))
    elsif request.fullpath == "/my_profile?draft"
      @ideas = policy_scope(Idea).where(profile_id: @profile.id).where(status: "brouillon").order(created_at: :desc)
    end
  end

  def show
    if Profile.find_by(user_id: current_user.id) == Profile.find(params[:id])
      redirect_to my_profile_path
    end
    authorize @profile
    if request.fullpath == "/profiles/#{params["id"]}" || request.fullpath == "/profiles/#{params["id"]}?ideas"
      @ideas = policy_scope(Idea).where(profile_id: @profile.id).where(status: "publié").order(created_at: :desc)
    elsif request.fullpath == "/profiles/#{params["id"]}?box_idea"
      @boxes = policy_scope(Box).where(profile_id: @profile.id)
    elsif request.fullpath == "/profiles/#{params["id"]}?pro_profiles"
      @pro_profiles = policy_scope(ProProfile).where(user_id: User.find(@profile.user_id))
    elsif request.fullpath == "/profiles/#{params["id"]}?draft"
      @ideas = policy_scope(Idea).where(profile_id: @profile.id).where(status: "brouillon").order(created_at: :desc)
    end
  end

  def new
    @profile = Profile.new
    authorize @profile
  end

  def create
    @user = User.find(current_user.id)
    @profile = Profile.new(profile_params)
    authorize @profile
    @profile.user_id = @user.id
    @profile.bookmark = []
    if !@profile.photo && @profile.sexe == "Homme"
      @profile.photo = "man.png"
    elsif !@profile.photo && @profile.sexe == "Femme"
      @profile.photo = "woman.png"
    end
    if @profile.valid?
      @profile.save
      redirect_to root_path
    else
      render :new
    end
  end

  def states
    skip_authorization
    country = params[:country]
    states = CS.states(country)
    render :json => states
  end

  def cities
    skip_authorization
    state = params[:state]
    cities = CS.cities(state)
    render :json => cities
  end

  def edit
    authorize @profile
  end

  def update
    @profile.update(profile_params)
    authorize @profile
    if @profile.save
      redirect_to my_profile_path
    else
      render :new
    end
  end

  def destroy
    authorize @profile
    @profile.destroy
  end

  private

  def profile_params
    params.require(:profile).permit(:city, :username, :age, :sexe, :photo, :country, :state)
  end

  def set_profile
    @profile = Profile.find(params[:id])
  end
end
