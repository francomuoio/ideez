class BookmarksController < ApplicationController

  before_action :set_bookmark, only: [:show, :edit, :update, :destroy ]

  def index
    @profile = Profile.find_by(user_id: current_user.id)
    @bookmarks = policy_scope(Bookmark).where(profile_id: @profile.id)
  end

  def show
    authorize @bookmark
  end

  def new
    @bookmark = Bookmark.new
    authorize @bookmark
    @profile = Profile.find_by(user_id: current_user.id)
    @idea = Idea.find(params["format"].to_i)
    @bookmark = Bookmark.new(profile_id: @profile.id, idea_id: @idea.id)
    authorize @bookmark
    if Bookmark.find_by(profile_id: @profile.id) && Bookmark.find_by(idea_id: @idea.id)
      redirect_to root_path
    else @bookmark.valid?
      @bookmark.save
      redirect_to root_path
    end
  end

  def destroy
    authorize @bookmark
    @bookmark.destroy
    redirect_to request.referrer
  end

  private

  def set_bookmark
    @bookmark = Bookmark.find(params[:id])
  end
end
