class ReviewsController < ApplicationController
  before_action :set_review, only: [:show, :edit, :update, :destroy]
  # before_action :set_user_and_profile_for_navbar

  def new
    @idea = Idea.find(params[:idea_id])
    @review = Review.new
    authorize @review
  end

  def create
    @user = User.find(current_user.id)
    @profile = Profile.find_by(user_id: @user.id)
    @review = Review.new(review_params)
    @idea = Idea.find(params[:idea_id])
    @review.profile_id = @profile.id
    @review.idea_id = @idea.id
    @review.upvote = 0
    @review.users_array = []
    authorize @review
    if @review.valid?
      @review.save
      redirect_to idea_path(@idea)
    else
      redirect_to (idea_path(@idea))
      flash[:alert] = "Creation Error"
    end
  end

  def show
    authorize @review
  end

  def upvote
    @review = Review.find(params[:format].to_i)
    @idea = Idea.find(@review.idea_id)
    @user = User.find(current_user.id)
    @profile = Profile.find(@review.profile_id)
    vote_found = false
    authorize @review
    if @review.users_array.include?([@user.id.to_s, @review.id.to_s])
      @review.upvote -= 1
      @review.users_array.delete([@user.id.to_s, @review.id.to_s])
    else
      @review.upvote += 1
      @review.users_array << [@user.id.to_i, @review.id.to_i]
    end
    @review.save!
    redirect_to idea_path(@idea)
  end

  def edit
    @idea = Idea.find(params[:idea_id])
    authorize @review
  end

  def update
    @idea = Idea.find(params[:idea_id])
    authorize @review
    if @review.update(review_params)
      redirect_to idea_path(@idea)
    else
      flash[:alert] = "Update Error"
      render 'edit'
    end
  end

  def destroy
    authorize @review
    @review.destroy
    redirect_to idea_path(params[:idea_id].to_i)
  end

  private

  def review_params
    params.require(:review).permit(:description, :photo)
  end

  def set_review
    @review = Review.find(params[:id])
  end

  # def set_user_and_profile_for_navbar
  #   @user = User.find(current_user.id)
  #   @profile = Profile.find(@user.id)
  # end
end
