class IdeasController < ApplicationController
  before_action :set_idea, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
  skip_before_action :verify_authenticity_token, raise: false

  def index
    if current_user
      @profile = Profile.find_by(user_id: current_user.id)
      @pro_profile = ProProfile.find_by(user_id: current_user.id)
    end
    if params[:query].present?
      sql_query = " \
        ideas.title ILIKE :query \
        OR ideas.description ILIKE :query \
        OR ideas.country ILIKE :query \
        OR ideas.state ILIKE :query \
        OR ideas.city ILIKE :query \
      "
      @ideas = policy_scope(Idea).where(status: "publié").where(sql_query, query: "%#{params[:query]}%")
    else
      @ideas = policy_scope(Idea).where(status: "publié")
    end
    if request.post?
      if params[:city]
       @ideas = @ideas.where(city: params[:city])
      elsif params[:state]
        @ideas = @ideas.where(state: params[:state])
      elsif params[:country]
        @ideas = @ideas.where(country: params[:country])
      end
    end
    if params[:order] == "date"
      @ideas = @ideas.order(created_at: :desc)
    elsif params[:order] == "popularity"
      @ideas_and_number_of_vote = []
      @ideas = @ideas.each do |idea|
        @ideas_and_number_of_vote << [ Rate.where(rateable_id: idea.id).count, idea]
      end
      @ideas = []
      @ideas_and_number_of_vote.sort.reverse.each do |idea|
        @ideas << idea[1]
      end
    elsif params[:order] == "rating"
      @ideas_and_avg_rating = []
      @ideas = @ideas.each do |idea|
        @ideas_and_avg_rating << [ Rate.avg(idea) + Rate.score_range(idea), idea]
      end
      @ideas = []
      @ideas_and_avg_rating.sort.reverse.each do |idea|
        @ideas << idea[1]
      end
    else
      @ideas = @ideas.order(created_at: :desc)
    end
  end

  def index_rating
    @ideas_and_avg_rating = []
    policy_scope(Idea).where(status: "publié").each do |idea|
      @ideas_and_avg_rating << [ Rate.avg(idea), idea]
    end
    @ideas = []
    @ideas_and_avg_rating.sort.reverse.each do |idea|
      @ideas << idea[1]
    end
  end

  def show
    @review = Review.new
    authorize @idea
  end

  def new
    @profile = Profile.find_by(user_id: current_user.id)
    @idea = Idea.new
    authorize @idea
  end

  def create
    @user = User.find(current_user.id)
    @profile = Profile.find_by(user_id: @user.id)
    @idea = Idea.new(idea_params)
    authorize @idea
    @idea.profile_id = @profile.id
    @idea.rating = 0
    if publishing?
      @idea.status = "publié"
    elsif saving_as_draft?
      @idea.status = "brouillon"
    end
    if @idea.valid?
      @idea.save
      redirect_to idea_path(@idea)
    else
      render :new
    end
  end

  def edit
    @profile_tags = ProfileTag.all.where(idea_id: @idea.id)
    @idea_boxes = IdeaBox.all.where(idea_id: @idea.id)
    authorize @idea
  end

  def update
    @idea.update(idea_params)
    @idea.boxes.clear
    if @idea.country == ""
      @idea.country = nil
    end
    if @idea.state == ""
      @idea.state = nil
    end
    if @idea.city == ""
      @idea.city = nil
    end
    if params[:idea][:box_ids]
      params[:idea][:box_ids].each do |id|
        @idea.boxes << Box.find(id)
      end
    end
    @idea.pro_profiles.clear
    if params[:idea][:pro_profile_ids]
      params[:idea][:pro_profile_ids].each do |id|
        @idea.pro_profiles << ProProfile.find(id)
      end
    end
    authorize @idea
    @idea.rating = 0

    if @idea.status == "publié" && params[:commit] == "Mettre à jour" || @idea.status == "publié" && params[:commit] == "Edit"
      @idea.status = "publié"
    elsif @idea.status == "brouillon" && params[:commit] == "Mettre à jour"
      @idea.status = "publié"
    elsif @idea.status == "publié" && params[:commit] == "Sauvegarder"
      @idea.status = "brouillon"
    elsif @idea.status == "brouillon" && params[:commit] == "Sauvegarder"
      @idea.status = "brouillon"
    end

    if @idea.save
      redirect_to idea_path(@idea)
    else
      redirect_to edit_idea_path(@idea)
    end
  end

  def destroy
    authorize @idea
    @idea.destroy
    redirect_to ideas_path
  end

  private

  def idea_params
    params.require(:idea).permit(:title, :description, :rating, :status, :photo, :country, :state, :city, box_ids: [], pro_profile_ids: [])
  end

  def set_idea
    @idea = Idea.find(params[:id])
  end

  def publishing?
    params[:commit] == "Postez"
  end

  def saving_as_draft?
    params[:commit] == "Sauvegarder"
  end
end
