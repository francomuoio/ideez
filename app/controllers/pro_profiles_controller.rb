class ProProfilesController < ApplicationController

  before_action :set_pro_profile, only: [:show, :edit, :update, :destroy ]

  def index
    if params[:query].present?
      sql_query = " \
        pro_profiles.title ILIKE :query \
        OR pro_profiles.bio ILIKE :query \
      "
      @pro_profiles = policy_scope(ProProfile).where(sql_query, query: "%#{params[:query]}%")
    else
      @pro_profiles = policy_scope(ProProfile)
    end
  end

  def show
    authorize @pro_profile
  end

  def new
    @pro_profile = ProProfile.new
    authorize @pro_profile
  end

  def create
    @user = User.find(current_user.id)
    @pro_profile = ProProfile.new(pro_profile_params)
    @pro_profile.user = @user
    authorize @pro_profile
    if @pro_profile.valid?
      @pro_profile.save
      redirect_to root_path
    else
      render :new
    end
  end

  def edit
    authorize @pro_profile
  end

  def update
    @pro_profile.update(pro_profile_params)
    authorize @pro_profile
    if @pro_profile.save
      redirect_to pro_profile_path(@pro_profile)
    else
      render :new
    end
  end

  def destroy
    authorize @pro_profile
    @pro_profile.destroy
  end

  def pro_profile_params
    params.require(:pro_profile).permit(:title, :bio, :photo)
  end

  def set_pro_profile
    @pro_profile = ProProfile.find(params[:id])
  end
end
