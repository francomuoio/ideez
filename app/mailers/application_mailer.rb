class ApplicationMailer < ActionMailer::Base
  default from: 'bonjour@bradway.com'
  layout 'mailer'
end
