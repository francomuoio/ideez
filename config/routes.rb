Rails.application.routes.draw do

  devise_for :users,
           :controllers  => {
             :registrations => 'my_devise/registrations',
             :omniauth_callbacks => 'users/omniauth_callbacks'

           }

  resources :bookmarks, only: [:index, :new, :destroy]

  post 'reviews/upvote'
  post 'ideas/filter', to: "ideas#index"

  scope '(:locale)', locale: /fr|en/ do
    mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
    post '/rate' => 'rater#create', :as => 'rate'
    # get 'bookmarks/index'
    # get 'bookmarks/new'
    # get 'bookmarks/destroy'


    resources :pro_profiles
    resources :boxes
    resources :ideas  do
      resources :reviews
    end



    get 'ideas/index_rating'

    resources :profiles

    # post 'profiles/bookmark'
    # post 'profiles/bookmarked_ideas'
    get 'states', to: 'profiles#states'
    get 'cities', to: 'profiles#cities'
    get 'my_profile', to: 'profiles#my_profile'

    get 'cgu', to: 'pages#cgu'


    root to: 'ideas#index'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
