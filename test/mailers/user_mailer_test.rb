require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
    user = User.first
    # This is how you pass value to params[:user] inside mailer definition!
    UserMailer.with(user: user).welcome
end
