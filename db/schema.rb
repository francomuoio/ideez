# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_20_180338) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "average_caches", id: :serial, force: :cascade do |t|
    t.integer "rater_id"
    t.string "rateable_type"
    t.integer "rateable_id"
    t.float "avg", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bookmarks", force: :cascade do |t|
    t.bigint "profile_id"
    t.bigint "idea_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["idea_id"], name: "index_bookmarks_on_idea_id"
    t.index ["profile_id"], name: "index_bookmarks_on_profile_id"
  end

  create_table "boxes", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.bigint "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["profile_id"], name: "index_boxes_on_profile_id"
  end

  create_table "idea_boxes", force: :cascade do |t|
    t.bigint "box_id"
    t.bigint "idea_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["box_id"], name: "index_idea_boxes_on_box_id"
    t.index ["idea_id"], name: "index_idea_boxes_on_idea_id"
  end

  create_table "ideas", force: :cascade do |t|
    t.bigint "profile_id"
    t.string "title"
    t.text "description"
    t.float "rating"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "profile_tag"
    t.string "status"
    t.string "photo"
    t.string "country"
    t.string "state"
    t.string "city"
    t.index ["profile_id"], name: "index_ideas_on_profile_id"
  end

  create_table "overall_averages", id: :serial, force: :cascade do |t|
    t.string "rateable_type"
    t.integer "rateable_id"
    t.float "overall_avg", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pro_profiles", force: :cascade do |t|
    t.bigint "user_id"
    t.string "title"
    t.text "bio"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "photo"
    t.index ["user_id"], name: "index_pro_profiles_on_user_id"
  end

  create_table "profile_tags", force: :cascade do |t|
    t.bigint "idea_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "pro_profile_id"
    t.index ["idea_id"], name: "index_profile_tags_on_idea_id"
    t.index ["pro_profile_id"], name: "index_profile_tags_on_pro_profile_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.bigint "user_id"
    t.string "city"
    t.string "username"
    t.integer "age"
    t.string "sexe"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.integer "bookmark", array: true
    t.string "photo"
    t.string "country"
    t.string "state"
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "rates", id: :serial, force: :cascade do |t|
    t.integer "rater_id"
    t.string "rateable_type"
    t.integer "rateable_id"
    t.float "stars", null: false
    t.string "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["rateable_id", "rateable_type"], name: "index_rates_on_rateable_id_and_rateable_type"
    t.index ["rater_id"], name: "index_rates_on_rater_id"
  end

  create_table "rating_caches", id: :serial, force: :cascade do |t|
    t.string "cacheable_type"
    t.integer "cacheable_id"
    t.float "avg", null: false
    t.integer "qty", null: false
    t.string "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["cacheable_id", "cacheable_type"], name: "index_rating_caches_on_cacheable_id_and_cacheable_type"
  end

  create_table "reviews", force: :cascade do |t|
    t.text "description"
    t.bigint "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "idea_id"
    t.integer "upvote"
    t.string "users_array", default: [], array: true
    t.string "photo"
    t.index ["idea_id"], name: "index_reviews_on_idea_id"
    t.index ["profile_id"], name: "index_reviews_on_profile_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "admin"
    t.string "provider"
    t.string "uid"
    t.string "facebook_picture_url"
    t.string "first_name"
    t.string "last_name"
    t.string "token"
    t.datetime "token_expiry"
    t.string "google_token"
    t.string "google_refresh_token"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "bookmarks", "ideas"
  add_foreign_key "bookmarks", "profiles"
  add_foreign_key "boxes", "profiles"
  add_foreign_key "idea_boxes", "boxes"
  add_foreign_key "idea_boxes", "ideas"
  add_foreign_key "ideas", "profiles"
  add_foreign_key "pro_profiles", "users"
  add_foreign_key "profile_tags", "ideas"
  add_foreign_key "profile_tags", "pro_profiles"
  add_foreign_key "profiles", "users"
  add_foreign_key "reviews", "ideas"
  add_foreign_key "reviews", "profiles"
end
