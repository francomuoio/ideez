class AddUpvoteToReview < ActiveRecord::Migration[5.2]
  def change
    add_column :reviews, :upvote, :integer
  end
end
