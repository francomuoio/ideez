class AddBookmarkToProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :bookmark, :integer, array: true
  end
end
