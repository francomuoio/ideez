class CreateProfileTags < ActiveRecord::Migration[5.2]
  def change
    create_table :profile_tags do |t|
      t.references :profile, foreign_key: true
      t.references :idea, foreign_key: true

      t.timestamps
    end
  end
end
