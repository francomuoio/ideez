class AddProProfileToProfileTag < ActiveRecord::Migration[5.2]
  def change
    add_reference :profile_tags, :pro_profile, foreign_key: true
  end
end
