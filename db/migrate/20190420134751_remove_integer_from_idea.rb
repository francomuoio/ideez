class RemoveIntegerFromIdea < ActiveRecord::Migration[5.2]
  def change
    remove_column :ideas, :integer, :string
  end
end
