class AddProfileTagToIdeas < ActiveRecord::Migration[5.2]
  def change
    add_column :ideas, :profile_tag, :string
    add_column :ideas, :integer, :string
  end
end
