class CreateProProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :pro_profiles do |t|
      t.references :user, foreign_key: true
      t.string :title
      t.text :bio

      t.timestamps
    end
  end
end
