class RemoveProfileTagFromIdea < ActiveRecord::Migration[5.2]
  def change
    remove_column :ideas, :profile_tag, :string
  end
end
