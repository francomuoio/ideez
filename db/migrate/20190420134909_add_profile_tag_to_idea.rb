class AddProfileTagToIdea < ActiveRecord::Migration[5.2]
  def change
    add_column :ideas, :profile_tag, :integer
  end
end
