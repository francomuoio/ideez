class AddUsersArrayToReview < ActiveRecord::Migration[5.2]
  def change
    add_column :reviews, :users_array, :string, array: true, default: []
  end
end
