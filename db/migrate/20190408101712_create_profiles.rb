class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.references :user, foreign_key: true
      t.string :city
      t.string :username
      t.integer :age
      t.string :sexe

      t.timestamps
    end
  end
end
