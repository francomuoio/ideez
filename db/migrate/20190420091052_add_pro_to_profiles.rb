class AddProToProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :pro, :string
    add_column :profiles, :boolean, :string
  end
end
