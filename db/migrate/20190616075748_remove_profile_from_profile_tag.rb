class RemoveProfileFromProfileTag < ActiveRecord::Migration[5.2]
  def change
    remove_reference :profile_tags, :profile, foreign_key: true
  end
end
