class AddCountryToIdea < ActiveRecord::Migration[5.2]
  def change
    add_column :ideas, :country, :string
  end
end
