class CreateIdeaBoxes < ActiveRecord::Migration[5.2]
  def change
    create_table :idea_boxes do |t|
      t.references :box, foreign_key: true
      t.references :idea, foreign_key: true

      t.timestamps
    end
  end
end
