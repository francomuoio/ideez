class AddPhotoToProProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :pro_profiles, :photo, :string
  end
end
