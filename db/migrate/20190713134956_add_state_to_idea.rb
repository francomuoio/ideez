class AddStateToIdea < ActiveRecord::Migration[5.2]
  def change
    add_column :ideas, :state, :string
  end
end
