class RemoveBooleanFromProfile < ActiveRecord::Migration[5.2]
  def change
    remove_column :profiles, :boolean, :string
  end
end
