class AddCityToIdea < ActiveRecord::Migration[5.2]
  def change
    add_column :ideas, :city, :string
  end
end
