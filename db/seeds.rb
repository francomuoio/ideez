# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "Creating users"

puts "user 1"
  user1 = User.create! :email => 'harry@gmail.com', :password => 'topsecret', :password_confirmation => 'topsecret'
  profile1 = Profile.create!(
      user_id: user1.id,
      username: "Harry",
      age: 29,
      sexe: "Un homme",
      city: "Paris",
      bookmark: []
    )

puts "user 2"
  user2 = User.create! :email => 'max@gmail.com', :password => 'topsecret', :password_confirmation => 'topsecret'
  profile2 = Profile.create!(
      user_id: user2.id,
      username: "Max",
      age: 38,
      sexe: "Un homme",
      city: "Lille",
      bookmark: []
    )
  pro_profile2 = ProProfile.create!(
    user_id: user2.id,
    title: "Reebok",
    bio: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    )

puts "user 3"
  user3 = User.create! :email => 'jess@gmail.com', :password => 'topsecret', :password_confirmation => 'topsecret'
  profile3 = Profile.create!(
      user_id: user3.id,
      username: "Jess",
      age: 49,
      sexe: "Une femme",
      city: "Tournai",
      bookmark: []
    )
  pro_profile3 = ProProfile.create!(
    user_id: user3.id,
    title: "Nike",
    bio: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    )

puts "Creating ideas..."

puts "idea 1"
  Idea.create!(
    title: "Idée 1",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
    rating: 0,
    status: "publié",
    photo: "idea-1"
  )

puts "idea 2"
  Idea.create!(
    title: "Idée 2",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
    rating: 0,
    status: "publié",
    photo: "idea-2"
  )

puts "idea 3"
  Idea.create!(
    title: "Idée 3",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
    rating: 0,
    status: "publié",
    photo: "idea-3"
  )

puts "idea 4"
  Idea.create!(
    title: "Idée 4",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
    rating: 0,
    status: "publié",
    photo: "idea-4"
  )

puts "idea 5"
  Idea.create!(
    title: "Idée 5",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
    rating: 0,
    status: "publié",
    photo: "idea-5"
  )

puts "idea 6"
  Idea.create!(
    title: "Idée 6",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
    rating: 0,
    status: "publié",
    photo: "idea-6"
  )

puts "idea 7"
  Idea.create!(
    title: "Idée 7",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
    rating: 0,
    status: "publié",
    photo: "idea-7"
  )

puts "idea 8"
  Idea.create!(
    title: "Idée 8",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
    rating: 0,
    status: "publié",
    photo: "idea-8"
  )

puts "idea 9"
  Idea.create!(
    title: "Idée 9",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
    rating: 0,
    status: "publié",
    photo: "idea-1"
  )

puts "idea 10"
  Idea.create!(
    title: "Idée 10",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
    rating: 0,
    status: "publié",
    photo: "idea-2"
  )

puts "creating boxes..."

puts "box 1"
  Box.create!(
    title: "Sport",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
  )

puts "box 2"
  Box.create!(
    title: "Ecologie",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
  )

puts "box 3"
  Box.create!(
    title: "Politique",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    profile_id: 1,
  )
